Package __ggpmisc__ is a small set of extensions to ggplot2 (>= 2.0.0) which
we hope will be useful when plotting diverse types of data.

Please, see the web site [R4Photobiology](http://www.r4photobiology.info) for 
details and update notices. Other packages, aimed at easing photobiology-related
calculations including the quantification of biologically effective radiation
in meteorology are available as part of a suite described at the same
website.

The current release of __ggpmisc__ is available through [CRAN](https://cran.r-project.org/web/packages/ggpmisc/index.html) 
for R (>= 3.2.0). 
